# openDARTS

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8046982.svg)](https://doi.org/10.5281/zenodo.8046982) [![Latest Release](https://gitlab.com/open-darts/open-darts/-/badges/release.svg)](https://gitlab.com/open-darts/open-darts/-/releases) [![pipeline status](https://gitlab.com/open-darts/open-darts/badges/development/pipeline.svg)](https://gitlab.com/open-darts/open-darts/-/commits/development) [![pypi](https://img.shields.io/pypi/v/open-darts.svg?colorB=blue)](https://pypi.python.org/project/open-darts/)
[![RSD](https://img.shields.io/badge/rsd-openDARTS-00a3e3.svg)](https://research-software-directory.org/software/opendarts)

[openDARTS](https://darts.citg.tudelft.nl/) is a scalable parallel modeling framework and aims to accelerate the simulation performance while capturing multi-physics processes in geo-engineering fields such as hydrocarbon, geothermal, CO2 sequestration and hydrogen storage.

## Installation

openDARTS is available for Python 3.9 to 3.12.

```bash
pip install open-darts
```

To build openDARTS please check the [instructions in our wiki](https://gitlab.com/open-darts/open-darts/-/wikis/Build-instructions).

## Tutorials

Check the [tutorial section in the documentation](https://open-darts.gitlab.io/open-darts/getting_started/tutorial.html) and a [crash-course on openDARTS](https://gitlab.com/open-darts/open-darts-workshop).

Also to get started take a look at the different examples in [models](https://gitlab.com/open-darts/open-darts/-/tree/development/models?ref_type=heads).

More advanced examples of complex simulations with openDARTS can be found in [open-darts-models](https://gitlab.com/open-darts/open-darts-models).

## Documentation

For more information about how to get started visit the [documentation](https://open-darts.gitlab.io/open-darts/).

## Legal

Please refer to [LICENSE.md](LICENSE.md) for more information about the licensing of openDARTS.

## For developers, help and support

Check [how to contribute](https://gitlab.com/open-darts/open-darts/-/wikis/Contributing) in our [wiki](https://gitlab.com/open-darts/open-darts/-/wikis/home) where developers can find other pertinent information.

## How to cite

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8046982.svg)](https://doi.org/10.5281/zenodo.8046982)