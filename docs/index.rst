.. darts-package documentation master file, created by
   sphinx-quickstart on Mon Apr 25 13:51:17 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DARTS documentation!
###############################

.. toctree::
   :maxdepth: 2
   :caption: ABOUT DARTS

   about_darts/about_darts.md
   
.. toctree::
   :maxdepth: 2
   :caption: GETTING STARTED

   getting_started/installation.md
   getting_started/tutorial.md
   getting_started/example_models.md

.. toctree::
   :maxdepth: 2
   :caption: FOR DEVELOPERS
   
   for_developers/darts_gitlab_setup.md
   for_developers/configure_hardware.md

.. toctree::
   :maxdepth: 2
   :caption: TECHNICAL REFERENCE
   
   technical_reference/glossary.md
   technical_reference/reservoir.md

.. toctree::
   :maxdepth: 2
   :caption: API

   api.rst


   
