# Examples 

Different models from various workshops and publications are located in [open-darts-models](https://gitlab.com/open-darts/open-darts/-/tree/development/models). 
- [Publications](https://gitlab.com/open-darts/darts-models/-/tree/development/publications?ref_type=heads)
- [CCS](https://gitlab.com/open-darts/darts-models/-/tree/development/teaching/CCS_workshop?ref_type=heads)
- [Geothermal](https://gitlab.com/open-darts/darts-models/-/tree/development/teaching/EAGE?ref_type=heads)