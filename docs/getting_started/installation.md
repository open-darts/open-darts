# Installation of open-darts

## Using pip

We recommend to use a python environment. Then installing open-darts is as simple as:

```bash
pip install open-darts
```

To build open-darts from source go to [build instructions](https://gitlab.com/open-darts/open-darts/-/wikis/Build-instructions)
